import MCardProfile from "components/molecules/MCardProfile"
import MTabs from "components/molecules/MTabs"
import OExperience from "components/organize/OExperience"
import OOverview from "components/organize/OOverview"
import OProject from "components/organize/OProject"
import ORepositories from "components/organize/ORepositories"
import OSkills from "components/organize/OSkills"
import { useState } from "react"



const tabItems = [
  { name: 'Overview', key: 1 },
  { name: 'Repositories', key: 2 },
  { name: 'Projects', key: 3 },
  { name: 'Experience', key: 4 },
  { name: 'Skills', key: 5 },
]

export default function Home() {
  const [tab, setTab] = useState({})
  
  function optionTabItem(key)  {
    const isActive = tabItems.find(object => object.key === key)
    setTab(isActive)
  }

  function tabComponent() {
    switch (tab?.key) {
      case 1:
    return <OOverview tabItems={tabItems} />
    case 2:
      return <ORepositories />    
    case 3:
      return <OProject />  
    case 4:
      return <OExperience />
    case 5:
      return <OSkills />    
    default:
      return <OOverview tabItems={tabItems} />
    }
  }

  return (
    <div className="container">
      <div className="row mt-5">
        <div className="col-md-4">
          <MCardProfile />
        </div>
        
        <div className="col-md-8">
          <section>
            <MTabs tabItems={tabItems} tab={tab} onClick={(key) => optionTabItem(key)} />
          </section>
          {tabComponent()}
        </div>
      </div>
    </div>
  )
}
