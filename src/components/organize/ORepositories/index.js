import React from 'react'
import MoreIcon from 'utils/icons/MoreIcon'

export default function ORepositories(props) {
  return (
    <section className='container'>
      <div className="form-header input-group-sm d-flex gap-2 mt-3">
        <input
          type="text"
          className='form-control'
          placeholder="Find a repository..."
          aria-label="Username"
        />
        <div class="dropdown d-flex gap-2">
          <button
            class="btn btn-outline-secondary btn-sm dropdown-toggle"
            id="dropdownLanguage"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Language
          </button>
          <button
            class="btn btn-outline-secondary btn-sm dropdown-toggle"
            id="dropdownSort"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Sort
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownLanguage">
            <li className='dropdown-item'>Javascript</li>
            <li className='dropdown-item'>PHP</li>
            <li className='dropdown-item'>HTML</li>
            <li className='dropdown-item'>React Js</li>
            <li className='dropdown-item'>Vue Js</li>
            <li className='dropdown-item'>Angular Js</li>
          </ul>
        </div>
      </div>

      <hr />

      <section className='text-center'>
        <h5>infanthree doesn’t have any public repositories yet.</h5>
      </section>

      <section>
        <div class="card">
          <div class="card-body d-flex justify-content-between align-items-center">
            <div>
              <div className='d-flex align-items-center gap-3'>
                <p className='m-0 text-primary border-bottom'>React Beginner</p>
                <small className='border rounded px-3 text-secondary'>Public</small>
              </div>
              <p className='my-2 text-secondary'>aplikasi chatting konsultasi kesehatan dan info seputar kesehatan</p>
              <div className='d-flex align-items-center gap-3'>
                <small className='text-secondary'>JavaScript</small>
                <small className='text-secondary'>Updated 28 days ago</small>
              </div>
            </div>
            <span 
              id="dropwDownShowMore"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <MoreIcon />
            </span>
            <ul class="dropdown-menu" aria-labelledby="dropwDownShowMore">
              <li className='dropdown-item'>Preview</li>
              <li className='dropdown-item'>Code</li>
            </ul>
          </div>
        </div>
      </section>
    </section>
  )
}
