import parse from 'html-react-parser';
import React from 'react';
import contentExperience from 'src/utils/static/contentExperience.json';

export default function OPackages(props) {
  return (
    <div className='container'>
      <p className='my-3'>My Experience</p>

      <section>
        <article>
          {
            contentExperience.map((item, index) =>
              parse(item.paragraph)
            )
          }
        </article>
      </section>
    </div>
  )
}
