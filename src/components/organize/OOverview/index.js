export default function OOverview(props) {
  const { tabItems } = props || {}
  
  return (
    <section className="ps-3 mt-3">
      <p>Popular Repositories</p>
      
      <div className="row">
        {
          tabItems.map(({ key }) => (
            <div className="col-md-6" key={key}>
              <div class="card mb-4">
                <div class="card-body d-flex align-items-center justify-content-between">
                  <div>
                    <h6 class="card-title">Nextjs Tutorial for Begginner</h6>
                    <small class="card-text text-secondary">Just for learning and newbie programmer</small>
                  </div>
                  <small className="text-secondary border-bottom">Public</small>
                </div>
              </div>
            </div>
          ))
        }
      </div>
    </section>
  )
}
