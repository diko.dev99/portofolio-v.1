import React, { useEffect, useState } from 'react'
import contentProjects from 'utils/static/contentProjects.json'
import styles from './styles.module.css'

export default function OProject(props) {
  const [sliceContent, setSliceContent] = useState([])
  const [isLess, setIsLiss] = useState(false)
  
  useEffect(() => {
    if(contentProjects?.length > 7) {
      setSliceContent(contentProjects.slice(0, 7))
    }
  }, [])

  function showMore() {
    setIsLiss(!isLess)
    if(sliceContent.length === contentProjects.length) {
      setSliceContent(contentProjects.slice(0, 7))
    } else {
      setSliceContent(contentProjects)
    }
  }

  return (
    <section className='container'>
      <p className='my-3'>My Project</p>
      <section className='row'>
        {
          sliceContent?.map(({ id, status, label, time, tech }) =>
            <div className="col-md-6" key={id}>
              <div className={styles['custom-card']}>
                <div className='card-body'>
                  <p className='m-0'>
                    <strong>{label}</strong>
                  </p>
                  <div className="d-flex align-items-center justify-content-between">
                    <p className={styles['time']}>{time}</p>
                    <p className={styles['status-' + status]}>{status}</p>
                  </div>
                  <div className={styles['wrapper-tag']}>
                    {
                      tech.map((item, index) =>
                        <p key={index}>{item}</p>
                      )
                    }
                  </div>
                </div>
              </div>  
            </div>
          )
        }
        {
          contentProjects?.length > 7 && (
            <div className={styles['show-more']} onClick={showMore}>
              <p className='text-success'>
                Show { isLess ? 'less' : 'More' } ...
              </p>
            </div>
          )
        }
      </section>
    </section>
  )
}
