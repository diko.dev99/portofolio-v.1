import React from 'react'

export default function MTabs(props) {
  const { tabItems, tab, onClick } = props || {}

  return (
    <ul class="nav nav-pills gap-3 border-bottom pb-2">
      {
        tabItems.map(({ name, key }) => {
          return (
            <li class="nav-item" key={key} onClick={() => onClick(key)}>
              <span
                class={`rounded-none ${tab.key === key ? 'border-bottom bg-primary rounded text-white' : ''} gap-2 px-3 py-2`}
                aria-current="page"
              >
                {name}
              </span>
            </li>
          )
        })
      }
    </ul>
  )
}
