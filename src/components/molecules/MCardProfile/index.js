import Image from "next/image"

export default function MCardProfile() {
  return (
    <section className="border border-secondary p-5 rounded">
      <div className="text-center">
        <Image
          src="/Images/il-me.jpg"
          alt="testing"
          fill="responsive"
          width={250}
          height={250}
          className="rounded-pill"
        />
      </div>
      <figure>
        <blockquote class="blockquote">
          <p>Diko Mahendra</p>
        </blockquote>
        <figcaption class="blockquote-footer">
          I'am  <cite title="Source Title">Frontend Engineer & Mobile Developer</cite>
        </figcaption>
      </figure>

      <div class="d-grid">
        <button type="button" className="btn btn-block btn-outline-secondary">
          Detail Profile
        </button>
      </div>

      <section className="d-flex gap-3 justify-content-center mt-2 text-secondary">
        <p>2 followers</p>
        <p>5 following</p>
      </section>

      <section className="border-bottom">
        <p>QODR</p>
        <p>Bantul Yogyakarta Indonesia</p>
        <p>https://medium.com/@cikodiko12</p>
      </section>

      <section>
        <p>Achievment</p>
      </section>
    </section>
  )
}
